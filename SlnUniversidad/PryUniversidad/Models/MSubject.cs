using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Universidad.DTO;

namespace Universidad.Models
{

    public class MSubject
    {
        public MSubject()
        {

        }

        public ResponseDTO AddSubject(SubjectDTO subject) {

            String queryInsert = "INSERT INTO public.tbl_subject(sub_name, sub_credit, dep_id) VALUES ('" + subject.nombre + "', '" + subject.creditos + "', '" + subject.department.id + "') returning sub_id;";
            MData data = new MData();     
            ResponseDTO responseBD = data.execute(queryInsert);  
            JArray array = JArray.Parse(responseBD.data);
            JObject dep = JObject.Parse(Convert.ToString(array[0]));
            subject.id = Convert.ToString(dep["sub_id"]);



            return new ResponseDTO(true, JsonConvert.SerializeObject(subject), ""); 
        }

        public ResponseDTO AssignDepartment(AssignDepDTO assign) {

            String queryUpdate = "UPDATE public.tbl_subject SET dep_id = '" + assign.dep_id + "' WHERE sub_id = '" + assign.sub_id + "';";
            MData data = new MData();     
            ResponseDTO responseBD = data.execute(queryUpdate);  
            return new ResponseDTO (true, JsonConvert.SerializeObject(assign), ""); 
        }

        public ResponseDTO DeleteSubject(int id) {

            String queryDelete = "DELETE FROM public.tbl_subject WHERE sub_id = '" + id +"';";
            MData data = new MData();     
            ResponseDTO responseBD = data.execute(queryDelete);  
            return new ResponseDTO (true, "", ""); 
        }

        public ResponseDTO GetSubject() {
            String query = "SELECT * FROM public.tbl_subject;";
            MData data = new MData();
            ResponseDTO responseBD = data.execute(query);
            return responseBD;
        }

        public ResponseDTO GetSubject(string id) {
            string query = "select subj.sub_id, subj.sub_name, subj.sub_credit, dep.dep_id, dep.dep_name from public.tbl_subject as subj, public.tbl_department as dep where subj.dep_id = dep.dep_id and subj.dep_id = '" + id + "'";
            MData data = new MData();
            ResponseDTO responseBD = data.execute(query);
            return responseBD;

        }
    }
}
using Universidad.DTO;
using Npgsql;
using System.Data;
using Newtonsoft.Json;

namespace Universidad.Models
{
    public class MData {
        
        public MData() {

        }

        public ResponseDTO execute(string command) {
            ResponseDTO response = new ResponseDTO(true, "", "");

            try
            {   
                Console.WriteLine(command);
                // Creacion de la conexion
                NpgsqlConnection conn = new NpgsqlConnection("Server = 209.50.55.238:97; User Id = postgres; Password = 5492bbf20; Database = postgres");
                conn.Open();

                // Definimos el query
                NpgsqlCommand query = new NpgsqlCommand(command, conn);

                // Ejecutar query
                NpgsqlDataReader dr = query.ExecuteReader();

                var dataTable = new DataTable();
                dataTable.Load(dr);

                response.data = JsonConvert.SerializeObject(dataTable);



                return response;
            }
            catch (System.Exception)
            {
                
                response.response = false;
                response.error = "Error en la conexión";
                return response;
            }

        }
    }

    
}
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Universidad.DTO;

namespace Universidad.Models
{

    public class MDepartment
    {
        public MDepartment()
        {

        }

        public ResponseDTO AddDepartment(DepartmentDTO department) {

            String queryInsert = "INSERT INTO public.tbl_department(dep_name) VALUES ('" + department.nombre + "') returning dep_id;";
            MData data = new MData();     
            ResponseDTO responseBD = data.execute(queryInsert);  
            JArray array = JArray.Parse(responseBD.data);
            JObject dep = JObject.Parse(Convert.ToString(array[0]));
            department.id = Convert.ToString(dep["dep_id"]);



            return new ResponseDTO(true, JsonConvert.SerializeObject(department), ""); 
        }
    }
}
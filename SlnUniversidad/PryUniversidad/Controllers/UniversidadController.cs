using Microsoft.AspNetCore.Mvc;
using Universidad.DTO;
using Universidad.Models;
using Newtonsoft.Json;

namespace Universidad.Controllers 
{
    [ApiController]
    [Route("apiuniversidad/")]
    public class UniversidadController : Controller
    {

        public UniversidadController() 
        {
        }

        // [HttpGet("getdepartments")]
        // public async Task<IActionResult> GetDepartments() {
        //     DepartmentDTO department = new DepartmentDTO("1", "Ingeniería");
        //     MateriaDTO materia = new MateriaDTO("2", "Programación", 3, department);
        //     return Ok(materia);
        // }

        [HttpPost("adddepartment")]
        public async Task<IActionResult> AddDepartment(DepartmentDTO department) {
            MDepartment mDepartment = new MDepartment();
            return Ok(mDepartment.AddDepartment(department));
        }

        [HttpPost("addsubject")]
        public async Task<IActionResult> AddSubject(SubjectDTO subject) {
            MSubject mSubject = new MSubject();
            return Ok(mSubject.AddSubject(subject));
        }

        [HttpPatch("assigndepartment")]
        public async Task<IActionResult> AssignDepartment(AssignDepDTO assign) {
            MSubject mSubject = new MSubject();
            return Ok(mSubject.AssignDepartment(assign));

        }

        [HttpDelete("deletesubject/{id}")]
        public async Task<IActionResult> DeleteSubject(int id) {
            MSubject mSubject = new MSubject();
            return Ok(mSubject.DeleteSubject(id));

        }

        [HttpGet("getsubject")]
        public async Task<IActionResult> GetSubject() {
            MSubject mSubject = new MSubject();
            return Ok(mSubject.GetSubject());
        }

        [HttpGet("getsubject/{id}")]
        public async Task<IActionResult> GetSubject(string id) {
            MSubject mSubject = new MSubject();
            return Ok(mSubject.GetSubject(id));
        }

    }
}
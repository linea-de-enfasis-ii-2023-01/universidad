using System.ComponentModel.DataAnnotations;

namespace Universidad.DTO
{
    public class DepartmentDTO
    {
        public string? id { get; set; }

        [Required(ErrorMessage = "El campo nombre es obligatorio")]
        public string nombre { get; set; }

        public DepartmentDTO(string? id, string nombre) {
            this.id = id;
            this.nombre = nombre;
        }
    }
}
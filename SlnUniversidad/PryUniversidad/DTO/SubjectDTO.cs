namespace Universidad.DTO
{
    public class SubjectDTO 
    {
        public string id { get; set; }

        public string nombre { get; set; }

        public int creditos { get; set; }

        public DepartmentDTO department { get; set; }

        public SubjectDTO(string id, string nombre, int creditos, DepartmentDTO department)
        {
            this.id = id;
            this.nombre = nombre;
            this.creditos = creditos;
            this.department = department;
        }


    }
}
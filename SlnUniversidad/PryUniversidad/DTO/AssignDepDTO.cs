namespace Universidad.DTO
{
    public class AssignDepDTO 
    {
        public string dep_id { get; set; }

        public string sub_id { get; set; }

        public AssignDepDTO(string dep_id, string sub_id)
        {
            this.dep_id = dep_id;
            this.sub_id = sub_id;
        }


    }
}
namespace PryUniversidad.test;
using Universidad.Models;
using Universidad.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class UnitTest1
{
    [Fact]
    public void GetSubjectSuccess()
    {
        MSubject mSubject = new MSubject();
        ResponseDTO response = mSubject.GetSubject();
        Assert.Equal(true, response.response);
    }

    [Fact]
    public void AddDepartmentSucess()
    {
        DepartmentDTO department = new DepartmentDTO(null, "Educación");
        MDepartment mDep = new MDepartment();
        ResponseDTO response = mDep.AddDepartment(department);
        Assert.Equal(true, response.response);

        // Assert.Equal("texto", response.data);
        JObject dep = JObject.Parse(Convert.ToString(response.data));
        string id = Convert.ToString(dep["id"]);

        Assert.Equal("texto", id);

        
    }

    public void AddDepartmentFail()
    {
        DepartmentDTO department = new DepartmentDTO(null, "Educación");
        MDepartment mDep = new MDepartment();
        ResponseDTO response = mDep.AddDepartment(department);
        Assert.Equal(true, response.response);

        // Assert.Equal("texto", response.data);
        JObject dep = JObject.Parse(Convert.ToString(response.data));
        string id = Convert.ToString(dep["id"]);

        Assert.Equal("texto", id);

        
    }
}